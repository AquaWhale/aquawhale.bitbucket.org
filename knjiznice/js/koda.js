
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
    sessionId = getSessionId();
	var vzorcni = [{ime: "Michael", priimek: "Jackson", teza: "76", visina: "185", temperatura: "40"},
	    {ime: "Debbie", priimek: "Rowe", teza: "55", visina: "175", temperatura: "37"},
	    {ime: "Paris", priimek: "Jackson", teza: "51", visina: "171", temperatura: "38"}
	    ];
	for (var i = 0; i < 3; i++) {
	    var ime = vzorcni[i].ime;
    	var priimek = vzorcni[i].priimek;
        var teza = vzorcni[i].teza;
        var visina = vzorcni[i].visina;
        var temperatura = vzorcni[i].temperatura;
        console.log (ime, priimek, teza, visina, temperatura);
    
    	$.ajaxSetup({
    	    headers: {"Ehr-Session": sessionId}
    	});
    	$.ajax({
    	    url: baseUrl + "/ehr",
    	    type: 'POST',
    	    success: function (data) {
    	        var ehrId = data.ehrId;
    	        var partyData = {
    	            firstNames: ime,
    	            lastNames: priimek,
    	            partyAdditionalInfo: [
    	                {key: "ehrId", value: ehrId},
    	                {key: "teza", value: teza},
    	                {key: "visina", value: visina},
    	                {key: "temperatura", value: temperatura}
    	                ]
    	        };
    	        $.ajax({
    	            url: baseUrl + "/demographics/party",
    	            type: 'POST',
    	            contentType: 'application/json',
    	            data: JSON.stringify(partyData),
    	            error: function(err) {
    	            	alert ("Novega pacienta ni bilo mogoče generirati, poskusite ponovno!");
    	            }
    	        });
    	        console.log (ehrId);
    	        var element = document.getElementById(ime);
    	        console.log(element);
    	        element.value = ehrId;
    	    }
    	});
	}
  return ehrId;
}

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datum = $("#kreirajDatumRojstva").val();
    var teza = $("#kreirajTezo").val();
    var visina = $("#kreirajVisino").val();
    var temperatura = $("#kreirajTelesnoTemperaturo").val();
    

	if (!ime || !priimek || !teza || !visina || !temperatura || !datum || ime.trim().length == 0 ||
      priimek.trim().length == 0 || teza.trim().length == 0 || visina.trim().length == 0 || 
      temperatura.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        console.log (ehrId);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [
		                {key: "ehrId", value: ehrId},
		                {key: "teza", value: teza},
		                {key: "visina", value: visina},
		                {key: "temperatura", value: temperatura}
		                ]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				var element = "<span class='obvestilo label label-success fade-in'>Pacient: "+party.firstNames+ " "+party.lastNames+"</span>  <span class='obvestilo label label-success fade-in'>Datum rojstva: "+party.dateOfBirth+"</span>";
	    	    for (var i = 0; i < party.partyAdditionalInfo.length; i++) {
	    	        switch (party.partyAdditionalInfo[i].key) {
	    	            case 'teza':
	    	                element += "  <span class='obvestilo label label-success fade-in'>Teža: "+party.partyAdditionalInfo[i].value+"</span>";
	    	                break;
	    	            case 'visina':
	    	                element += "  <span class='obvestilo label label-success fade-in'>Višina: "+party.partyAdditionalInfo[i].value+"</span>";
	    	                break;
	    	            case 'temperatura':
	    	                element += "  <span class='obvestilo label label-success fade-in'>Temperatura: "+party.partyAdditionalInfo[i].value+"</span>";
	    	                break;
	    	        }
	    	    }
	    	    element += "<p style='color:orange'>Spodaj si oglejte grafični prikaz vitalnih znakov ter najbližje lokacije bolnišnic.</p>";
	    	    $("#preberiSporocilo").html(element);
	    	    
	    	},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function poisciLokacijo () {
    
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            izdelajZemljevid(position);
        });
    } else {
        $("#map").innerHTML = "Trenutne lokacije ni bilo možno pridobiti";
    }
}

function izdelajZemljevid(lokacija) {
    var map;
    var service;
    var infowindow;
    var pyrmont = new google.maps.LatLng(lokacija.coords.latitude, lokacija.coords.longitude);
    
    map = new google.maps.Map(document.getElementById('map'), {
          center: pyrmont,
          zoom: 13
    });
    
    var marker = new google.maps.Marker ({
        position: pyrmont,
        map: map,
        icon: {
            url:"https://maps.google.com/mapfiles/kml/paddle/blu-circle.png",
            scaledSize: new google.maps.Size(40,40)
        }    
    });
    
    var request = {
        location: pyrmont,
        radius: '2400',
        type: ['hospital']
    };
    
    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                var position = results[i].geometry.location
                var marker = new google.maps.Marker ({
                    position: position,
                    map: map,
                    icon: {
                        url:"https://maps.google.com/mapfiles/kml/shapes/hospitals.png",
                        scaledSize: new google.maps.Size (40,40)
                    }
                });
                var distance = 30;
                var bolnisnica = "<div>"+results[i].name+"</div>"
                $("#bolnisnice").append(bolnisnica);
            } 
        }
    });
}

$(document).ready(function() {
  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajTezo").val(podatki[3]);
    $("#kreirajVisino").val(podatki[4]);
    $("#kreirajTelesnoTemperaturo").val(podatki[5]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
});